//
//  Models.swift
//  petApp
//
//  Created by Matran, Bogdan on 05/06/2018.
//  Copyright © 2018 Matran, Bogdan. All rights reserved.
//

import Foundation
import UIKit

enum Type {
    case carnivores
    case herbivores
}

enum WaterType {
    case salty
    case fresh
}

protocol Animal {
    var lives: UInt { get set }
    var name: String { get set}
    var gender: String { get set }
    var age: Int { get set }
    var animalImage: UIImage { get set}
    
    func makeNoise() -> String
    
    func description() -> String
}

class Fish: Animal {
    var animalImage: UIImage = #imageLiteral(resourceName: "animal contour stamp")
    var lives: UInt
    var name: String
    var gender: String
    var age: Int
    var waterType: WaterType
    func makeNoise() -> String {
       return "Blup Blup"
    }
    init(lives: UInt, name: String, gender: String, age: Int, waterType: WaterType) {
        self.lives = lives
        self.age = age
        self.gender = gender
        self.name = name
        self.waterType = waterType
    }
    func description() -> String {
        return String(describing: Fish.self)
    }
}

class Dolphin: Fish {
    var isBlue: Bool
    
    override func makeNoise() -> String {
        return "I am a dolphin"
    }
    
    init(isBlue: Bool, lives: UInt, name: String, gender: String, age: Int, waterType: WaterType) {
        self.isBlue = isBlue
        super.init(lives: lives, name: name, gender: gender, age: age, waterType: waterType)
    }
    
    override func description() -> String {
        return String(describing: Dolphin.self)
    }
}

class Trench: Fish {
    var dimensions: (UInt, UInt)
    
    override func makeNoise() -> String {
        return "I am a fresh water fish!!"
    }
    
    init(dimensions: (UInt, UInt), lives: UInt, name: String, gender: String, age: Int, waterType: WaterType) {
        self.dimensions = dimensions
        super.init(lives: lives, name: name, gender: gender, age: age, waterType: waterType)
    }
    
    override func description() -> String {
        return String(describing: Trench.self)
    }
}

class Mammal: Animal {
 
    var animalImage: UIImage = #imageLiteral(resourceName: "animal contour stamp")
    var lives: UInt
    var name: String
    var gender: String
    var limbs: UInt
    var age: Int
    var numberOfBirths: UInt
    var species: Type
    
    func makeNoise() -> String {
        return "Mammal"
    }
    
    func description() -> String {
        return String(describing: Mammal.self)
    }
    
    init(lives: UInt, limbs: UInt, name: String, gender: String, age: Int, numberOfBirths: UInt, species: Type) {
        self.age = age
        self.lives = lives
        self.gender = gender
        self.name = name
        self.species = species
        self.limbs = limbs
        self.numberOfBirths = numberOfBirths
    }
}

class Dog: Mammal {
    var color: String
    
    init(lives: UInt, limbs: UInt, color: String, age: Int, name: String, gender: String,numberOfBirths: UInt, species: Type) {
        self.color = color
        super.init(lives: lives, limbs: limbs, name: name, gender: gender, age: age, numberOfBirths: numberOfBirths, species: species)
    }
    
    override func makeNoise() -> String {
       return "Ham Ham"
    }
    
    override func description() -> String {
        return String(describing: Dog.self)
    }
}

class Cat: Mammal {
    var breed: String
    
    override func makeNoise() -> String {
        return "Meow"
    }
    
    init(breed: String, lives: UInt, limbs: UInt, name: String, gender: String, age: Int, numberOfBirths: UInt, species: Type) {
        self.breed = breed
        super.init(lives: lives, limbs: limbs, name: name, gender: gender, age: age, numberOfBirths: numberOfBirths, species: species)
    }
    
    override func description() -> String {
        return String(describing: Cat.self)
    }
}

class Hamster: Mammal {
    var cageSize: UInt
    
    init(cageSize: UInt, lives: UInt, limbs: UInt, name: String, gender: String, age: Int, numberOfBirths: UInt, species: Type) {
        self.cageSize = cageSize
        super.init(lives: lives, limbs: limbs, name: name, gender: gender, age: age, numberOfBirths: numberOfBirths, species: species)
    }
    
    override func description() -> String {
        return String(describing: Hamster.self)
    }
}

class Bird: Animal {
  
    var animalImage: UIImage = #imageLiteral(resourceName: "animal contour stamp")
    var lives: UInt
    var limbs: UInt
    var color: String
    var age: Int
    var name: String
    var gender: String
    var canFly: Bool
    
    func makeNoise() -> String {
       return "Buk Buk"
    }
  
    func description() -> String {
        return String(describing: Bird.self)
    }
    
    init(lives: UInt, limbs: UInt, color: String, age: Int, name: String, gender: String, canFly: Bool) {
        self.limbs = limbs
        self.lives = lives
        self.color = color
        self.age = age
        self.name = name
        self.gender = gender
        self.canFly = canFly
    }

}

class Parrot: Bird {
    var cageSize: UInt
    
    override func makeNoise() -> String {
        return "Speaks like a human"
    }
    
    init(cageSize: UInt,lives: UInt, limbs: UInt,  color: String, age: Int, name: String, gender: String, canFly: Bool) {
        self.cageSize = cageSize
        super.init(lives: lives, limbs: limbs, color: color, age: age, name: name, gender: gender, canFly: canFly)
    }

    override func description() -> String {
        return String(describing: Parrot.self)
    }
}

class Chicken: Bird {
    var eggsADay: UInt
    
    override func makeNoise() -> String {
       return "I am a chicken "
    }
    
    init(eggsADay: UInt,lives: UInt, limbs: UInt, color: String, age: Int, name: String, gender: String, canFly: Bool) {
        self.eggsADay = eggsADay
        super.init(lives: lives, limbs: limbs, color: color, age: age, name: name, gender: gender, canFly: canFly)
        
    }
    override func description() -> String {
        return String(describing: Chicken.self)
    }
}





