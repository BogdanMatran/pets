//
//  AnimalsViewController.swift
//  petApp
//
//  Created by Matran, Bogdan on 05/06/2018.
//  Copyright © 2018 Matran, Bogdan. All rights reserved.
//

import UIKit

class AnimalsViewController: UIViewController {
    
    @IBOutlet weak var animalsTableView: UITableView!
    
    var animalArray = [Animal]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        animalsTableView.dataSource = self
        animalsTableView.register(UINib(nibName: "AnimalsTableViewCell", bundle: nil), forCellReuseIdentifier: AnimalsTableViewCell.cellIdentifier())
        animalsTableView.separatorStyle = .none
        createDataSource()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "Pets to adopt"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let possition = sender as! IndexPath
        let animalToSend = animalArray[possition.row]
        let destinationController: DetailsViewController = segue.destination as! DetailsViewController
        destinationController.animal = animalToSend
    }
    
    func createDataSource() {
        let cat = Cat(breed: "Persian", lives: 9, limbs: 4, name: "Kitty", gender: "Female", age: 3, numberOfBirths: 0, species: .carnivores)
        cat.animalImage = #imageLiteral(resourceName: "4-ways-cheer-up-depressed-cat")
        let dog = Dog(lives: 1, limbs: 4, color: "Black", age: 4, name: "Doggy", gender: "Male", numberOfBirths: 0, species: .carnivores)
        dog.animalImage = #imageLiteral(resourceName: "download")
        let derp = Cat(breed: "Scotish", lives: 9, limbs: 4, name: "Derpy", gender: "Male", age: 2, numberOfBirths: 0, species: .carnivores)
        derp.animalImage = #imageLiteral(resourceName: "maxresdefault")
        let bird = Bird(lives: 1, limbs: 2, color: "Grey", age: 2, name: "Cipy", gender: "Female", canFly: true)
        bird.animalImage = #imageLiteral(resourceName: "bird")
        let dolphin = Dolphin(isBlue: true, lives: 1, name: "Dolpho", gender: "Male", age: 5, waterType: .fresh)
        dolphin.animalImage = #imageLiteral(resourceName: "Delphin")
        let animalsToAdd: [Animal] = [cat, dog, derp, bird, dolphin]
        animalArray.append(contentsOf: animalsToAdd)
    }    
}

extension AnimalsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return animalArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: AnimalsTableViewCell.cellIdentifier(), for: indexPath) as! AnimalsTableViewCell
        cell.decorate(animal: animalArray[indexPath.row])
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "transitionToDetails", sender: indexPath)
    }
}
