//
//  AnimalsTableViewCell.swift
//  petApp
//
//  Created by Matran, Bogdan on 05/06/2018.
//  Copyright © 2018 Matran, Bogdan. All rights reserved.
//

import UIKit

class AnimalsTableViewCell: UITableViewCell {

    @IBOutlet weak var animalImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var genderAgeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        animalImageView.addShadow(with: 10, and: 0.75)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    class func  cellIdentifier() -> String {
        return "AnimalTableViewCell"
    }
    
    func decorate(animal: Animal) {
        nameLabel.text = animal.name
        genderAgeLabel.text = "Age \(animal.age), Gender \(animal.gender)"
        animalImageView.image = animal.animalImage
    }
    
}
