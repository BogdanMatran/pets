//
//  DetailsViewController.swift
//  petApp
//
//  Created by Matran, Bogdan on 31/05/2018.
//  Copyright © 2018 Matran, Bogdan. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController {

    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var topImageView: UIImageView!
    @IBOutlet weak var bottomButton: UIButton!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var sexAgeLabel: UILabel!
    @IBOutlet weak var noiseButton: UIButton!
    
    var animal: Animal?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let a = animal {
            decorate(animal: a)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        topImageView.addShadow(with: 15, and: 0.75)
        bottomButton.addShadow(with: 10, and: 0.75)
        noiseButton.addShadow(with: 10, and: 0.75)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        addCornerRadiusTo(view: noiseButton, with: 6)
        addCornerRadiusTo(view: bottomButton, with: 6)
    }
    
    @IBAction func didTapOnNoiseButton(_ sender: Any) {
        noiseAlert()
    }
    
    func decorate(animal: Animal) {
        nameLabel.text = "This is \(animal.name) it is a \(animal.description()) that is looking for a owner."
        sexAgeLabel.text = "It is a \(animal.gender) and has the age of \(animal.age)."
        topImageView.image = animal.animalImage
    }
    
    func addCornerRadiusTo(view: UIView, with: CGFloat) {
        view.layer.cornerRadius = with
    }
    
    func noiseAlert() {
        let alert = UIAlertController(title: "Pet says", message: animal?.makeNoise(), preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            switch action.style{
            case .default:
                print("default")
                
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
            }}))
        self.present(alert, animated: true, completion: nil)
    }
}

extension UIView {
    func addShadow(with radius: CGFloat, and opacity: Float) {
        self.layer.shadowOffset = CGSize.zero
        self.layer.shadowRadius = radius
        self.layer.shadowColor = UIColor.white.cgColor
        self.layer.shadowOpacity = opacity
    }

}
